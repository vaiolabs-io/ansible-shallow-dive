# Ansible Shallow Dive


- Intro:
  - History
  - Configuration Management
  - Environment Architecture

- Setup:
  - Environment Discussion
  - Install and Setup

- Ansible Fundaments:

  - Initial Config
  - Ansible Commands
  - Ansible Modules
  - Ansible Plugins
  - Ansible Playbooks

- Ansible Playbooks:
  - Yaml Basics
    - Structure Of Ansible Playbook
    - Ansible Modules In Playbooks
  - Storing And Passing Information With In Playbook
  - Variables
  - Conditional Execution in Playbooks
  - Looping Through Variables And Lists

- Ansible Roles:
  - Converting To Roles
  - Files and Templates
  - Tasks and Handlers
  - Encrypting Things In Ansible
  - Including Other Playbooks

- Ansible Advance Execution
  - Limits
  - Tags
  - Idempotence:
    - changed_when
    - failed_when

- Troubleshooting Testing And Validation:
  - Troubleshooting issues
  - Jumping To Specific
  - Debugging

---
© All Right reserved to Alex M. Schapelle of VaioLabs ltd.
